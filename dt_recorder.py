import pyaudio
import wave
import datetime
import numpy as np
import sys, os
from getopt import getopt
import samplerate as sr

import time
from soundrecorder.recorder_config import RecorderConfig

import logging as LOG
# setup "global" logger
if not os.path.exists('logs'):
   os.makedirs('logs')
LOG.basicConfig(filename='logs/dt_recorder.log',
                filemode='a',
                format='%(asctime)s %(levelname)s: %(message)s')

def mkdir_folder(folder_name):
   if os.path.exists(folder_name):
      LOG.warning('path exists %s' % (folder_name))
   else:
      LOG.warning('path does not exist %s' % (folder_name))
      os.makedirs(folder_name)  # similar to "mkdir -p"

   return
                           
def check_disk_space():
   os_platform = 'nt'  # for windows OS
   disk_space_left = 1

   if os.name == 'posix':
      os_platform = 'ix'  # Linux, including Mac
      st = os.statvfs('.')
      disk_space_left = st.f_frsize * st.f_bavail # in bytes
   else: # Windows, our target platform
      free_bytes = ctypes.c_ulonglong(0)
      ctypes.windll.kernel32.GetDiskFreeSpaceExW(
         ctypes.c_wchar_p('.'), None, None, ctypes.pointer(free_bytes))
      disk_space_left = free_bytes.value # in bytes

   disk_space_left //= 1073741824 # in GB, 1073741824 = 1024^3
   if disk_space_left < 5:  # in GigaBytes
      print('Disk space has only %d (GB) remaining.' % (disk_space_left))
   elif disk_space_left < 10:
      print('Disk space has %d (GB)remaining.' % (disk_space_left))
                                                             
   return

def check_hardware_resources(p, device_index):
   p_info = p.get_device_info_by_index(device_index)
   q = pyaudio.PyAudio()
   q_info = q.get_device_info_by_index(device_index)
   for k in p_info:
      if p_info[k] != q_info[k]:
         print('Recording device altered')
         sys.exit(1)
   print('Recording device OK')
   return

def save2file(fil, frames, fs=16000, channels=1, sample_format=2):
   pass

def usage():
   print('qrecorder_rate.py -r <rate> -i <deviceindex> ' +
         '-o <outfile> -d <dura> -c <channels> -n <new_rate> -t (turn on debug)')
   print('   where <rate> is for recording, and')
   print('   <new_rate> is for file output, after down/up sampling')

def business_hours(cfg):
   LOG.debug(cfg.start_time, cfg.end_time)
   tid = cfg.start_time.split(':')
   if len(tid) != 2:
      print('ERROR in business hours, start_time=%s', cfg.start_time)
      sys.exit(-10)
   start_time = float(tid[0]) + float(tid[1])/60
   tid = cfg.end_time.split(':')
   if len(tid) != 2:
      print('ERROR in business hours, end_time=%s', cfg.end_time)
   end_time = float(tid[0]) + float(tid[1])/60
   LOG.debug(start_time, end_time)
   return start_time, end_time

def parse_cmdline(argv, cfg):
   try:
      opts, args = getopt(argv, "hr:i:o:d:tc:n:",
                   ['rate', 'deviceindex', 'outfile', 'dura', 'channels', 'new_rate'])
   except getopt.GetoptError:
      usage()
      sys.exit(1)
   except:
      usage()
      sys.exit(1)
   
   opt_dict = dict()
   opt_dict['device_index'] = cfg.device_index
   opt_dict['rate'] = cfg.fs
   opt_dict['new_rate'] = cfg.new_fs
   opt_dict['dura'] = cfg.dura # secs
   opt_dict['channels'] = cfg.channels
   opt_dict['channels_dist'] = cfg.channels_dist
   opt_dict['outfile'] = 'qrecorder_rate.wav'
   opt_dict['toprint'] = False

   try:
      for opt, arg in opts:
         if opt in ("-h"):
            usage()
            sys.exit(1)
         if opt in ("-r", "--rate"):
            opt_dict['rate'] = int(arg)
         if opt in ("-n", "--new_rate"):
            opt_dict['new_rate'] = int(arg)
         if opt in ("-i", "--deviceindex"):
            #print('arg=', arg)
            opt_dict['device_index'] = int(arg)
            #print('ARG=', opt_dict['device_index'])
         if opt in ("-o", "--outfile"):
            opt_dict['outfile'] = arg
         if opt in ("-d", "--dura"):
            opt_dict['dura'] = float(arg)
         if opt in ("-c", "--channels"):
            opt_dict['channels'] = int(arg)
         if opt in ("-t"):
            opt_dict['toprint'] = True
   except ValueError as err:
      print('ValueError, err=', err)
      print('Please check your command line.')
      sys.exit(-1)

   return opt_dict

# START.../main()
def main(argv):
   chunk = 1024
   format = pyaudio.paInt16
   config = RecorderConfig('dt_recorder.ini')
   opt_dict = parse_cmdline(argv[1:], config)
   rate = int(opt_dict['rate'])
   new_rate = int(opt_dict['new_rate'])
   device_index = opt_dict['device_index']
   record_seconds = int(opt_dict['dura'])
   outfile = opt_dict['outfile']
   toprint = opt_dict['toprint']
   channels = int(opt_dict['channels'])
   conversion_needed = False if rate == new_rate else True
   conversion_ratio = new_rate/rate
   if toprint:
      for k in opt_dict:
         print(k, opt_dict[k])
      print('device_index=', device_index, rate, record_seconds, outfile, toprint)
      print('conversion_needed=', conversion_needed, conversion_ratio, rate, new_rate)


   p = pyaudio.PyAudio()
   info = None
   try:
      info = p.get_device_info_by_index(device_index)
   except:
      pass

   if toprint:
      print('info=', info)

   if info == None or info['maxInputChannels'] < 1:  # FIXED
      print('ERROR: Selected device_index not available for recording')
      valid_ids = []
      for i in range(20):  # Some hard-coded number
         try:
            info = p.get_device_info_by_index(i)   # Very useful
         except:
            pass
         if info != None and info['maxInputChannels'] >= 1:
            valid_ids.append(i)

      if len(valid_ids) > 0:
         print('INFO: Available device_index:', valid_ids)
         sys.exit(-1)
      elif info['maxInputChannels'] < channels:
         print('ERROR, the selected device only has %d channels (%d needed)' %
               (info['maxInputChannels'], channels))
         sys.exit(-2)
      else:
         if toprint: print('p=', p)

   strem = None
   try:
      #print('Kolla', channels)
      stream = p.open(format=format,
                      channels=channels,
                      input_device_index=device_index,
                      rate=rate,
                      input=True,
                      frames_per_buffer=chunk)
   except OSError as err:
      print('Error in p.open(). err=', err)
      sys.exit(1)

   wf = []
   data_mine = []  # in nparray for up/down sampling of frequency
   resampler = []
   for ch in range((channels)):
      wf.append([])
      tmp = np.empty([0], dtype=np.int16)
      data_mine.append(tmp)
      if conversion_needed:
         resampler.append(sr.Resampler())
      if toprint:
         print('ch=', ch, resampler)

   tot = int(rate/chunk*(record_seconds/channels))
   if toprint:
      print('data_mine=', data_mine)
      print('wf=', wf, '*'*20)
      print('rate/chunk=%d rate/chunk*(record_seconds/channels)=%d' % (int(rate/chunk), tot))

   start_time, end_time = business_hours(config)
   session_counter = 0
   this_tid = datetime.datetime.now()
   this_day = this_tid.day
   final_audio_path = ''
   print("Recording starts {:%Y/%m/%d %H:%M:%S}". format(this_tid))

   # Recording forever...
   while True:
      tid = datetime.datetime.now()
      hour = int('{:2d}'.format(tid.hour))
      mint = int('{:2d}'.format(tid.minute))/60
      tid1 = hour + mint
      if tid1 < start_time:
         LOG.debug('too early')
         time.sleep(60)
         session_counter = 0
         continue
      elif tid1 > end_time:
         LOG.debug('we are closed')
         time.sleep(60)
         continue
      else:
         LOG.debug('we are open')
         curr_day = tid.day
         if curr_day != this_day:
            session_counter = 0
            this_day = curr_day
            LOG.debug('Date changed')
            check_disk_space()
            check_hardware_resources(p, device_index)

      dagtid = '{:%Y%m%d}' . format(tid)
      final_audio_path = config.audiopath+os.path.sep+dagtid
      mkdir_folder(final_audio_path)
      fil_prefix = "{:%Y-%m-%d_%H_%M}". format(tid)
      fil_prefix = '%s_S%02d_Ch' % (fil_prefix, session_counter)
      if record_seconds > 500: steps = 200
      elif record_seconds > 200: steps = 100
      else: steps = 20
      #print('fil_prefix=', fil_prefix)
      try:
         for i in range(0, tot):
            data = stream.read(chunk*channels, exception_on_overflow=False)
            # convert string to numpy array
            data_nparray = np.frombuffer(data, dtype='int16')
            for ch in range(channels):
               channel = data_nparray[ch::channels]
               #print('type', type(channel))
               if conversion_needed:
                  out = resampler[ch].process(channel, conversion_ratio, end_of_input=False)
                  channel = out.astype(np.int16)
                  #print('TYPE', type(channel))
               data_mine[ch] = np.concatenate((data_mine[ch], channel), axis=0)
               if toprint and i < 2:
                  print('shape=', data_mine[ch].shape)
               if i % (tot//steps) == 0:  # print at most 10 times per cycle
                  print("Recorded %.2f%%" % (100.*i/tot))
      except KeyboardInterrupt as err:
         print('Interruption ctrl-c received')
         sys.exit(1)

      print("Recording {:%Y/%m/%d %H:%M:%S}". format(datetime.datetime.now()))

      for ch in range(channels):
         if toprint: print('ch=', ch)
         fil = final_audio_path + os.path.sep + fil_prefix + '%02d.wav' % (ch)
         LOG.debug('fil=', fil)
         wf[ch] = wave.open(fil, 'wb')

         wf[ch].setnchannels(1)
         wf[ch].setsampwidth(p.get_sample_size(format))
         wf[ch].setframerate(new_rate)
         wf[ch].writeframes(b''.join(data_mine[ch]))
         wf[ch].close()

      for ch in range((channels)):
         data_mine[ch] = np.empty([0], dtype=np.int16)

      session_counter += 1
      # if session_counter > 2: break

   # end of while True

   # Now it will never reach here. Just a note
   stream.stop_stream()
   stream.close()
   p.terminate()

if __name__ == "__main__":
   main(sys.argv)
