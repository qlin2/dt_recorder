import sys, os
import configparser as cp

class RecorderConfig:
    logpath = 'logs'
    logfile = 'dt.log'
    filemode = 'a'
    audiopath = 'audiodata'
    bysession = -1
    bytime = 10
    channels = 4
    channels_dist = '0,1,2,4'
    device_index = 0
    fs = 16000
    new_fs = 16000
    dura = 3
    test = 'test'
    sanity_audio = 1
    customer_lanes = '0,1'
    gains = '2.1,1.'
    start_time = '10:10'
    end_time = '22:45'
    
    def __init__(self, fn):
        self.fname = fn
        configured = False
        err_msg = []
        if os.path.isfile(fn):
            config = []
            try:
                config = cp.ConfigParser()
                config.read(fn)
                #print(config.sections())
                configured = True
            except:
                s = 'Error encountered during configuration: %s.' % (fn)
                err_msg.append(s)
                print(s)

            try:
                self.logpath = config['LOG_FILE_SPEC']['logpath']
                self.logfile = config['LOG_FILE_SPEC']['logfile']
                self.filemode = config['LOG_FILE_SPEC']['filemode']
                self.audiopath = config['WHERE_TO_STORE']['audiopath']
                self.bysession = config['HOW_TO_BREAK_FILES']['bysession']
                self.bytime = config['HOW_TO_BREAK_FILES']['bytime']
                self.channels = config['RECORDER_SETTINGS']['device_index']
                self.channels = config['RECORDER_COMMON_SETTINGS']['channels']
                self.channels_dist = config['RECORDER_COMMON_SETTINGS']['channels_dist']
                self.fs = config['RECORDER_COMMON_SETTINGS']['fs']
                self.new_fs = config['RECORDER_COMMON_SETTINGS']['new_fs']
                self.dura = config['RECORDER_COMMON_SETTINGS']['dura']
                #self.test = config['RECORDER_COMMON_SETTINGS']['test']
                self.sanity_audio = config['SANITY_CHECK']['sanity_audio']
                self.customer_lanes = config['LANE_DISTRIBUTION']['customer']
                self.gains = config['LANE_DISTRIBUTION']['gains']
                self.start_time = config['BUSINESS_HOURS']['start_time']
                self.end_time = config['BUSINESS_HOURS']['end_time']
            except KeyError as err:
                print('err=', err)
                s = 'Error during reading ini file: %s' % (fn)
                err_msg.append(s)
                print(s)
                configured = False

        else:
            s = 'Error configuration file: %s, not found.' % (fn)
            err_msg.append(s)
            print(s)
            configured = False

        self.configured = configured
        self.err_msg = err_msg

    def config_string(self):
        s = 'logpath=%s logfile=%s filemode=%s audiopath=%s bysession=%s bytime=%s '
        s += 'channels=%s c_d%s fs=%s (%s) seconds=%s sanity_audio=%s customer=%s gains=%s'
        s = (s % (self.logpath, self.logfile, self.filemode, self.audiopath, self.bysession, self.bytime,
                  self.channels, self.channels_dist, self.fs, self.new_fs, self.dura, self.sanity_audio,
                  self.customer_lanes, self.gains))
        return s


if __name__ == "__main__":
    rc = RecorderConfig('dt_recorder.ini')
    print('rc.filemode=', rc.filemode, 'rc.configured=', rc.configured)
    print('length of err message:', len(rc.err_msg))
    for i in range(len(rc.err_msg)):
        print('msg=', rc.err_msg[i])

    #print(rc.toString())
    print(eval("RecorderConfig"))
    print(rc.config_string())

    print('gains=', rc.gains)
    gains = [1., 1.,]
    gains[0] = float(rc.gains.split(',')[0].strip())
    print('gain0=', gains[0])
    gains[1] = float(rc.gains.split(',')[1].strip())
    print('gain1=', gains[1])
    print('g0*g1=', gains[0] * gains[1])
